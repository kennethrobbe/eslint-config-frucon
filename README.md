# eslint-config-frucon

This package provides the ESLint configuration used for projects at [Frucon�](https://www.frucon.net). It's based on the [airbnb-base](https://github.com/airbnb/javascript) ESLint config.

Make sure to install all peer dependencies when using this package.
