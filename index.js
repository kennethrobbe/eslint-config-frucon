module.exports = {
    'env': {
        'node': true,
    },
    'extends': 'airbnb-base',
    'rules': {
        'indent': ['error', 4, { 'SwitchCase': 1 }],
        'no-console': 'off',
        'prefer-destructuring': 'off',
        'dot-notation': 'off',
        'one-var': 'off',
        'one-var-declaration-per-line': 'off',
        'valid-jsdoc': 'error',
    },
    'parserOptions': {
        'sourceType': 'script',
        'ecmaVersion': 9,
    }
};